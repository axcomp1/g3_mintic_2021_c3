# Generated by Django 3.2.8 on 2021-10-21 00:36

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authApp', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='catalogocompra',
            name='cliente',
        ),
        migrations.AddField(
            model_name='catalogocompra',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='catalogocompra',
            name='item',
            field=models.ManyToManyField(to='authApp.Producto'),
        ),
        migrations.AlterField(
            model_name='producto',
            name='imagen',
            field=models.CharField(max_length=200),
        ),
    ]
