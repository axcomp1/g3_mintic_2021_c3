from django.db import models

from authApp.models.producto import Producto
#rom .catalogoCompra import CatalogoCompra


class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60, null=False)
    cedula = models.IntegerField(null=False)
    direccion = models.CharField(max_length=60, null=False)
    #catalogo = models.ManyToManyRel(Producto,CatalogoCompra)

    def __str__(self):
        return str(self.id)+ " "+ str(self.nombre)