from typing import Optional
from rest_framework import views
from rest_framework.response import Response
from authApp.models.producto import Producto
from authApp.serializers.productoSerializer import MostrarProductoSerializer, CrearProductoSerializer, ActualizarProductoSerializer


class ProductView(views.APIView):
    def get(self, request, pk=0):
        if pk is not None:
            try:
                producto = Producto.objects.get(pk=pk)
                serializer = MostrarProductoSerializer(producto)
                return Response(serializer.data, 200)
            except:
                lista_productos = Producto.objects.all()
                serializer = MostrarProductoSerializer(lista_productos, many=True)
                return Response(serializer.data, 200)
        #return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = CrearProductoSerializer(data=datos_json)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "producto creado"}, 200)
        else:
            return Response(serializer.errors, 400)

    def put(self,request,pk):
        datos_json=request.data
        #producto = self.get_object(pk)
        producto = Producto.objects.get(pk=pk)
        serializer = ActualizarProductoSerializer(producto, data=datos_json)
        try:
            
            if serializer.is_valid():
                serializer.save()
                #return Response({"mensaje": "producto actualizado"}, 200)
                return Response(serializer.data)
        except:
            return Response({"mensaje": "el producto no pudo actualizarse"}, 400)


    def delete(self, request, pk):
        try:
            producto = Producto.objects.get(pk=pk)
            producto.delete()
            return Response({"mensaje": "producto eliminado"}, 204)
        except:
            return Response({"mensaje": "ha ocurrido un error eliminando el producto"}, 400)
''' 
Ejemplo de JSON para poner POST en la pagina de /producto:
{
    "nombre": "Carriel de cable Ethernet",
    "precio": 50000,
    "imagen": "https://bit.ly/2YNWCjJ",
    "stock": 50
}


{"cliente": 1, "items":[{"id":1},
{"id":2}]
}
'''