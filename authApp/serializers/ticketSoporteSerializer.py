from rest_framework import serializers
from authApp.models.ticketSoporte import TicketSoporte

class CrearTicketSoporteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketSoporte
        fields = ["usuario","pqrs"]

class MostrarTicketSoporteSerializer(serializers.ModelSerializer):
    class Meta:
        model = TicketSoporte
        fields = ["id", "usuario","pqrs"]
