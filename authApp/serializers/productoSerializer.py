from rest_framework import serializers
from authApp.models.producto import Producto

class CrearProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ["nombre", "precio", "imagen","descuento","stock"]

class MostrarProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ["id", "nombre", "precio", "imagen","descuento","stock"]

class ActualizarProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ["id", "nombre", "precio", "imagen","descuento","stock"]

class MostrarProductoCatalogoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ["id","nombre","precio", "imagen","stock"]