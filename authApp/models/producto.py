from django.db import models

class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60, null=False)
    precio = models.IntegerField(null=False)
    imagen = models.CharField(max_length=200, null=False)
    descuento = models.BooleanField(default=False)
    stock=models.IntegerField(null=False)


