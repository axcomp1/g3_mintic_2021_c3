from rest_framework import serializers
from authApp.models.catalogoCompra import CatalogoCompra
from .productoSerializer import MostrarProductoCatalogoSerializer
from authApp.models.producto import Producto
from authApp.models.cliente import Cliente
from .clienteSerializer import MostrarClienteCatalogoSerializer


class CrearCatalogoCompraSerializer(serializers.ModelSerializer):
     #items = MostrarProductoCatalogoSerializer(many=True)
     #cliente=MostrarClienteCatalogoSerializer()
     '''def create(self,validated_data):
         #print(type(validated_data["cliente"]))
         #print(validated_data["cliente"])
         cliente=validated_data["cliente"]
         productos = validated_data["items"]
         print(validated_data["items"])
         #for e in validated_data["items"]:
            #producto=Producto.objects.get(pk=e["id"])
            #print(e)
            #productos.append(producto)
         
         catalogo=CatalogoCompra(cliente=cliente)
         catalogo.save()
         catalogo.items.set(productos)
         catalogo.save()
         return catalogo'''
     class Meta:
         model = CatalogoCompra
         #fields = ["cedula", "item","descontable","producto"]
         #fields = ["cliente","items"]
         fields = ["cliente","items"]
         

class ActualizarCatalogoCompraSerializer(serializers.ModelSerializer):
     #items = MostrarProductoCatalogoSerializer(many=True)
     #cliente = MostrarClienteCatalogoSerializer(serializers.ModelSerializer)
     class Meta:
         model = CatalogoCompra
         #fields = ["id","cliente","items","fecha"]
         fields = ["items"]


class MostrarCatalogoCompraSerializer(serializers.ModelSerializer):
     items = MostrarProductoCatalogoSerializer(many=True)
     cliente = MostrarClienteCatalogoSerializer(serializers.ModelSerializer)
     class Meta:
         model = CatalogoCompra
         fields = ["id","cliente","items","fecha"]