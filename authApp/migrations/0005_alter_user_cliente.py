# Generated by Django 3.2.8 on 2021-10-27 17:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authApp', '0004_user_cliente'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='cliente',
            field=models.ForeignKey(max_length=100, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cliente', to='authApp.cliente'),
        ),
    ]
