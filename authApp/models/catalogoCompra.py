from django.db import models
from .producto import Producto
from .cliente import Cliente
from datetime import datetime

class CatalogoCompra(models.Model):
    id = models.AutoField(primary_key=True)
    items=models.ManyToManyField(Producto, unique=False)
    fecha = models.DateTimeField(default = datetime.now)
    cliente = models.ForeignKey(Cliente, related_name='carrito',on_delete=models.CASCADE)
    
    
    #cliente = models.ForeignKey(Cliente,on_delete=models.CASCADE)
    #item = models.ForeignKey(Producto, related_name='Producto.id',on_delete=models.CASCADE)
    #item=models.ManyToManyRel(Producto.id,Cliente)
    #cedula = models.ForeignKey(Cliente,related_name='authApp.Cliente.cedula',on_delete=models.CASCADE)
    #item = models.ForeignKey(Producto, related_name='authApp.producto.id',on_delete=models.CASCADE)
    #descontable = models.ForeignKey(Producto, related_name='authApp.Producto.descuento',on_delete=models.CASCADE)
    #un catalogo de compras úede tener muchos productos, guardarlos en un array? cómo instanciarlos?