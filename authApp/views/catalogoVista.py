from rest_framework import views
from rest_framework.response import Response
from authApp.models.cliente import Cliente
from authApp.models.producto import Producto
from authApp.models.catalogoCompra import CatalogoCompra
from authApp.serializers.productoSerializer import MostrarProductoSerializer
from authApp.serializers.catalogoCompraSerializer import ActualizarCatalogoCompraSerializer, CrearCatalogoCompraSerializer, MostrarCatalogoCompraSerializer
from authApp.serializers.clienteSerializer import MostrarClienteCatalogoSerializer
from .productoVista import ProductView

class CatalogueView(views.APIView):
    def get(self, request, pk=0):
        if pk is not None:
            try:
                catalogo = CatalogoCompra.objects.get(pk=pk)
                serializer = MostrarCatalogoCompraSerializer(catalogo)
                #print(serializer.data.items()['cliente'])
                print(serializer.data.items())
                print('\n')
                for e in serializer.data.items():
                    print(e)
                #cliente = Cliente.objects.get(id=catalogo["cliente"].id)
                print('\n')
                print(catalogo)
                #cliente = Cliente.objects.all()
                #cliente = Cliente.objects.get(pk=serializer.data["cliente"].id)
                #for e in serializer.data.items:
                    #print(e)
                #ser2 = MostrarClienteCatalogoSerializer(cliente)
                #print(cliente)
                #print(ser2)

                #lista_catalogos = CatalogoCompra.objects.all()
                #print(lista_catalogos.get(0))
                return Response(serializer.data, 200)
            except:
                lista_catalogos = CatalogoCompra.objects.all()
                serializer = MostrarCatalogoCompraSerializer(lista_catalogos, many=True)
                return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = CrearCatalogoCompraSerializer(data=datos_json)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "Se registro un nuevo Catalogo"}, 200)
        else:
            return Response(serializer.errors, 400)

    def delete(self, request, pk):
        try:
            catalogo = CatalogoCompra.objects.get(pk=pk)
            catalogo.delete()
            return Response({"mensaje": "el catalogo ha sido eliminado"}, 204)
        except:
            return Response({"mensaje": "ha ocurrido un error eliminando el registro del catalogo"}, 400)


    def put(self,request,pk):
        datos_json=request.data
        #catalogo = self.get_object(pk)
        catalogo = CatalogoCompra.objects.get(pk=pk)
        serializer = ActualizarCatalogoCompraSerializer(catalogo, data=datos_json)
        print(datos_json["items"])
        try:
            if serializer.is_valid():
                serializer.save()
                #return Response({"mensaje": "catalogo actualizado"}, 200)
                return Response(serializer.data)
        except:
            return Response({"mensaje": "el catalogo no pudo actualizarse"}, 400)