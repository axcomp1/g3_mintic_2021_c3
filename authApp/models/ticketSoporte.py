from django.db import models
from .cliente import Cliente

class TicketSoporte(models.Model):
    id = models.AutoField(primary_key=True)
    usuario = models.ForeignKey(Cliente, related_name='ticket',on_delete=models.CASCADE)
    #usuario = models.ForeignKey(Cliente,on_delete=models.CASCADE)
    pqrs = models.CharField(max_length=280, null=False)

    