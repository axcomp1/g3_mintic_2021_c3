from django.contrib import admin

from authApp.models.producto import Producto
from authApp.models.cliente import Cliente
from authApp.models.user import User
#from authApp.models.catalogoCompra import CatalogoCompra


admin.site.register(User)
#admin.site.register(CatalogoCompra)
admin.site.register(Producto)  #producto no estaba registrado
admin.site.register(Cliente)

