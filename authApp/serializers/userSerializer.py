from rest_framework import serializers
from authApp.models.user import User
from authApp.models.cliente import Cliente
from authApp.serializers.clienteSerializer import MostrarClienteSerializer

class UserSerializer(serializers.ModelSerializer):
    #cliente = MostrarClienteSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'cliente']

class UpdateClientUserSerializer(serializers.ModelSerializer):
    #cliente = MostrarClienteSerializer()
    class Meta:
        model = User
        fields = ['cliente']

class UpdatePasswordUserSerializer(serializers.ModelSerializer):
    #cliente = MostrarClienteSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'cliente']

    '''def create(self, validated_data):
        #clienteData = validated_data.pop('cliente')
        userInstance = User.objects.create(**validated_data)

        #Account.objects.create(user=userInstance, **accountData)
        return userInstance
    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        cliente = Cliente.objects.get(cliente=obj.id)
        return {
        'id': user.id,
        'username': user.username,
        'name': user.name,
        'email': user.email,
        'cliente':cliente,
    } '''