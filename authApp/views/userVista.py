from rest_framework import views
from rest_framework.response import Response
from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer, UpdateClientUserSerializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

class UserView(views.APIView):
    def get(self, request, pk=0):
        if pk is not None:
            try:
                user = User.objects.get(pk=pk)
                serializer = User(user)
                return Response(serializer.data, 200)
            except:
                lista_usuarios = User.objects.all()
                serializer = UserSerializer(lista_usuarios, many=True)
                return Response(serializer.data, 200)
        #return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = UserSerializer(data=datos_json)
        if serializer.is_valid():
            serializer.save()
            #a = User.objects.all
            return Response({"mensaje": "Se registro un nuevo Usuario"}, 200)
        else:
            return Response(serializer.errors, 400)

        ''' def put(self,request,pk):
        datos_json=request.data
        #producto = self.get_object(pk)
        user = User.objects.get(pk=pk)
        serializer = UserSerializer(user, data=datos_json)
        try:
            
            if serializer.is_valid():
                serializer.save()
                #return Response({"mensaje": "usuario actualizado"}, 200)
                return Response(serializer.data)
        except:
            return Response({"mensaje": "No se actualizo la información del usuario"}, 400)
''' 
    def put(self,request,pk):
        datos_json=request.data
        #producto = self.get_object(pk)
        user = User.objects.get(pk=pk)
        serializer = UpdateClientUserSerializer(user, data=datos_json)
        try:
            if serializer.is_valid():
                serializer.save()
                #return Response({"mensaje": "usuario actualizado"}, 200)
                return Response(serializer.data)
        except:
            return Response({"mensaje": "No se actualizo la información del usuario"}, 400)

    def delete(self, request, pk):
        try:
            user = User.objects.get(pk=pk)
            user.delete()
            return Response({"mensaje": "registro del cliente ha sido eliminado"}, 204)
        except:
            return Response({"mensaje": "ha ocurrido un error eliminando el registro del cliente"}, 400)