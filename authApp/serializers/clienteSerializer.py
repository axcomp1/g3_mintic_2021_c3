from rest_framework import serializers
from authApp.models.cliente import Cliente
#from .catalogoCompraSerializer import MostrarCatalogoCompraSerializer

class CrearClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ["nombre", "cedula", "direccion"]

class MostrarClienteSerializer(serializers.ModelSerializer):
    #compras = MostrarCatalogoCompraSerializer(many=True)
    class Meta:
        model = Cliente
        fields = ["id", "nombre", "cedula", "direccion"]

class ActualizarClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ["id", "nombre", "cedula", "direccion"]

class MostrarClienteCatalogoSerializer(serializers.ModelSerializer):
    #compras = MostrarCatalogoCompraSerializer(many=True)
    class Meta:
        model = Cliente
        fields = ["id", "nombre", "cedula"]