from typing import Optional
from rest_framework import views
from rest_framework.response import Response
from authApp.models.cliente import Cliente
from authApp.serializers.clienteSerializer import CrearClienteSerializer, MostrarClienteSerializer, ActualizarClienteSerializer
from authApp.serializers.catalogoCompraSerializer import CrearCatalogoCompraSerializer

class ClientView(views.APIView):
    def get(self, request, pk=0):
        if pk is not None:
            try:
                cliente = Cliente.objects.get(pk=pk)
                serializer = MostrarClienteSerializer(cliente)
                return Response(serializer.data, 200)
            except:
                lista_clientes = Cliente.objects.all()
                serializer = MostrarClienteSerializer(lista_clientes, many=True)
                return Response(serializer.data, 200)
        #return Response(serializer.data, 200)

    def post(self, request):
        datos_json = request.data
        serializer = CrearClienteSerializer(data=datos_json)
        if serializer.is_valid():
            serializer.save()
            return Response({"mensaje": "Se registro un nuevo Cliente"}, 200)
        else:
            return Response(serializer.errors, 400)

    def put(self,request,pk):
        datos_json=request.data
        #producto = self.get_object(pk)
        cliente = Cliente.objects.get(pk=pk)
        serializer = ActualizarClienteSerializer(cliente, data=datos_json)
        try:
            
            if serializer.is_valid():
                serializer.save()
                #return Response({"mensaje": "producto actualizado"}, 200)
                return Response(serializer.data)
        except:
            return Response({"mensaje": "No se actualizo la información del cliente"}, 400)


    def delete(self, request, pk):
        try:
            cliente = Cliente.objects.get(pk=pk)
            cliente.delete()
            return Response({"mensaje": "registro del cliente ha sido eliminado"}, 204)
        except:
            return Response({"mensaje": "ha ocurrido un error eliminando el registro del cliente"}, 400)